# Tenki

Tenki a simple PHP application to log current weather conditions and notes.

The accompanying shell script can be used to read the date of a specified photo then find a matching text file, and write its contents into the _Comment_ field of the photo.

## Author

Dmitri Popov [dmpop@linux.com](mailto:dmpop@linux.com)

## License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)
